# Performance Comparison of Some RL Libraries

## Install

```bash
$ nix-channel --update
$ git clone https://gitlab.com/harwiltz/rl-lib-benchmarks.git
$ cd rl-lib-benchmarks
$ nix-shell
[nix-shell]$ python cartpole/_coax.py
```

![](./coax-cartpole.png)
