{ lib
, buildPythonPackage
, fetchFromGitHub
, numpy
, jax
, jaxlib
, dm-haiku
, gym
, tqdm
, optax
, cloudpickle
, lz4
, pillow
, pandas
, tensorboardx
, protobuf
}:

buildPythonPackage rec {
  pname = "coax";
  version = "0.1.9";

  src = fetchFromGitHub {
    owner = "coax-dev";
    repo = pname;
    rev = "v${version}";
    sha256 = "1kqc9728qwlhmgd0phmmim21pvxy77w6rk6fjw7rkwgkslzrkmj3";
  };

  doCheck = false;

  postPatch = ''
    sed -i "s/^optax[>=]=.*$/optax/g" requirements.txt
    sed -i "s/^gym.*$/gym/" requirements.txt
    sed -i "/^tensorboard/d" requirements.txt
  '';

  pythonImportsCheck = [ "coax" ];

  propagatedBuildInputs = [
    dm-haiku
    jax
    jaxlib
    optax
    cloudpickle
    lz4
    pillow
    gym
    pandas
    tensorboardx
    protobuf
  ];
  meta = with lib;  {
    license = licenses.mit;
  };
}
