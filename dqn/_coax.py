import timeit
import logging
import argparse
import coax
import gym
import haiku as hk
import jax
import jax.numpy as jnp
import numpy as np
import matplotlib.pyplot as plt
from coax.value_losses import mse
from optax import adam

HIDDEN_SIZE = 32

def main(args):
    durs = (timeit.Timer(lambda: experiment(args.env,
                                            args.hidden,
                                            verbose=args.verbose,
                                            demo=args.demo))
            .repeat(repeat=args.samples,
                    number=args.reps_per_sample))
    mean_durs = np.array(durs).squeeze() / args.reps_per_sample
    plt.hist(mean_durs, density=True, bins=args.bins)
    plt.xlabel("Time")
    plt.ylabel("Density")
    plt.title(f"Time to learn optimal {args.env} policy via DQN in Coax")
    plt.show()

def experiment(env, hidden, verbose=False, demo=False):
    env = gym.make(env)
    env = coax.wrappers.TrainMonitor(env,
                                     name='dqn',
                                     tensorboard_dir='/tmp/tensorboard/coax',
                                     level=logging.INFO if verbose else logging.CRITICAL)

    def func(S, is_training):
        seq = hk.Sequential((
            hk.Linear(hidden), jax.nn.relu,
            hk.Linear(hidden), jax.nn.relu,
            hk.Linear(env.action_space.n, w_init=jnp.zeros)
        ))
        return seq(S)

    q = coax.Q(func, env)
    pi = coax.BoltzmannPolicy(q, temperature=0.1)

    q_targ = q.copy()

    tracer = coax.reward_tracing.NStep(n=1, gamma=0.99)
    buffer = coax.experience_replay.SimpleReplayBuffer(capacity=1000)

    qlearning = coax.td_learning.QLearning(q,
                                        q_targ=q_targ,
                                        loss_function=mse,
                                        optimizer=adam(1e-3))

    for ep in range(1000):
        s = env.reset()
        for t in range(env.spec.max_episode_steps):
            a = pi(s)
            s_next, r, done, info = env.step(a)

            if t == env.spec.max_episode_steps - 1:
                assert done
                r = 1/(1 - tracer.gamma)

            tracer.add(s, a, r, done)
            while tracer:
                buffer.add(tracer.pop())

            if len(buffer) >= 100:
                transition_batch = buffer.sample(batch_size=32)
                metrics = qlearning.update(transition_batch)
                env.record_metrics(metrics)

            q_targ.soft_update(q, tau=0.01)

            if done:
                break

            s = s_next

        if env.avg_G > env.spec.reward_threshold:
            while demo:
                view_demo(env, coax.EpsilonGreedy(q, epsilon=0.0))
                ans = input("Watch another? [y/n]").lower()
                if (len(ans) > 0) and (ans[0] == 'n'):
                    demo = False
            return
    raise ValueError("DQN didn't learn optimal policy!")

def view_demo(env, policy):
    s = env.reset()
    score = 0.
    for t in range(env.spec.max_episode_steps):
       a = policy(s)
       s, r, done, info = env.step(a)
       score += r
       env.render()
       if done:
           break
    env.close()
    print(f"Score: {score}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--samples",
                        type=int,
                        default=100,
                        help="Number of datapoints to plot")
    parser.add_argument("--reps_per_sample",
                        type=int,
                        default=1,
                        help="Number of times experiment should be run " + \
                             "for each datapoint. The datapoint plotted " + \
                             "will be the mean among these runs.")
    parser.add_argument("--bins",
                        type=int,
                        default=25,
                        help="Number of bins for histogram")
    parser.add_argument("--lr",
                        type=float,
                        default=1e-3,
                        help="Learning rate")
    parser.add_argument("--demo",
                        default=False,
                        action="store_true",
                        help="Watch demos of the agent (disrupts benchmark)")
    parser.add_argument("--verbose",
                        default=False,
                        action="store_true",
                        help="Log training metrics (disrupts benchmark)")
    parser.add_argument("--env",
                        type=str,
                        default="CartPole-v0",
                        help="Gym environment")
    parser.add_argument("--hidden",
                        type=int,
                        default=32,
                        help="Hidden layer size")
    args = parser.parse_args()
    main(args)
