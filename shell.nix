{ pkgs ? import <nixpkgs> {}
#, fetchFromGitHub
}:

let
  python = let
    packageOverrides = self: super: {
      # Disable chex tests, since they take long
      # This is mainly to reduce CI load
      chex = super.chex.overridePythonAttrs(old: rec {
        doCheck = false;
        propagatedBuildInputs = (
          old.propagatedBuildInputs ++ [super.jax super.jaxlib]
          );
      });

      dm-haiku = super.dm-haiku.overridePythonAttrs(old: rec {
        postPatch = ''
          rm -rf docs examples
        '';
        propagatedBuildInputs = (
          old.propagatedBuildInputs ++ [super.jax super.jaxlib]
          );
        doCheck = false;
      });

      optax = super.optax.overridePythonAttrs(old: rec {
        doCheck = false;
        postPatch = ''
          rm -rf docs examples
        '';
        propagatedBuildInputs = (
          old.propagatedBuildInputs ++ [super.jax super.jaxlib]
        );
      });
    };
  in pkgs.python3.override { inherit packageOverrides; };

  pps = python-packages: with python-packages; [
    dm-haiku
    optax
    gym
    matplotlib
    pyglet
    (callPackage ./coax.nix {})
  ];

  pyenv = python.withPackages pps;
in pkgs.mkShell {
  nativeBuildInputs = [ pyenv ];
}

